const express = require("express");
const router = express.Router();
const db = require("../models");
const { requireSignin } = require("../auth/passport");


//register subjects of the student
router.post("/registerStudentSubject", (req, res) => {
    db.Student_Subject.create({
        StudentId: req.body.studentid,
        SubjectId: req.body.subjectid,
    }).then(student => res.send(student));
});

//delete because of student
router.delete("/deleteStudentSubjects/:id",requireSignin, (req, res) => {
    db.Student_Subject.destroy({
        where: {
            StudentId: req.params.id
        }
    }).then(() => res.send({ message: "success" }));

});


//delete because of subject
router.delete("/deleteSubjects/:id",requireSignin, (req, res) => {
    db.Student_Subject.destroy({
        where: {
            SubjectId: req.params.id
        }
    }).then(() => res.send({ message: "success" }));

});

module.exports = router;