const express = require("express");
const router = express.Router();
const db = require("../models");
const { requireSignin } = require("../auth/passport");


//Register a new assignment
router.post("/registerAssignment", (req, res) => {
    db.Assignment.create({
        Due_Date: req.body.duedate,
        Marks: req.body.marks,
        ChapterId: req.body.chapterid,
        SubjectId: req.body.subjectid,
        TeacherId: req.body.teacherid,
        Branch: req.body.branch
    }).then(assignment => res.send(assignment));
});

//get assignment questions
router.get("/question/:assignmentid",requireSignin, (req, res) => {

    db.Question.findAll({
        where: {
            assignmentId: req.params.assignmentid
        },
    }).then(question => res.send(question));
}
);

//delete assignment
router.delete("/delete/assignment/:id",requireSignin, (req, res) => {
    db.Assignment.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));

});

//update due date of assignment
router.put("/updateDate",requireSignin, (req, res) => {
    db.Assignment.update(
        {
            Due_Date: req.body.duedate,
        },
        {
            where: {
                id: req.body.id
            }
        }).then((date) => res.send(date));
});

//update Marks of assignment
router.put("/updateMarks",requireSignin, (req, res) => {
    db.Assignment.update(
        {
            Marks: req.body.marks,
        },
        {
            where: {
                id: req.body.id
            }
        }).then((marks) => res.send(marks));
});



//get assignments by teachers
router.get("/getEverything/:id",requireSignin, (req, res) => {

    db.Assignment.findAll({
        where: {
            TeacherId: req.params.id
        },
        include: [

            {
                model: db.Chapter,
            },
            {
                model: db.Subject,
            }
        ]
    }).then(record => res.send(record)
    )
});

//get assignment by id
router.get("/getAssignment/:id",requireSignin, (req, res) => {

    db.Assignment.findOne({
        where: {
            id: req.params.id
        },
        include: [
            db.Chapter, db.Subject
        ]
    }).then(Quiz => res.send(Quiz)
    )
});
///////////////////////////////////////////////////

router.post("/getAssignmentStatusbySubjects",requireSignin, async (req, res) => {


    const student = await db.Student.findOne({
        where: {
            id: req.body.studentid
        }
    })

    if (!student) {
        return res.status(403).json({ message: "Student not found" });
    }

    const branch = await db.Branch.findOne({
        where: {
            id: req.body.branch
        }
    })
    if (branch.Status == 0) {
        return res.status(403).json({ message: "Branch deactivated" });
    }

    const subject = await db.Subject.findAll({
        where: {
            Class: req.body.class
        },
        include: [{
            model: db.Assignment,
            where: {
                Branch: req.body.branch
            },
            include: [db.Subject, db.Chapter]
        }
        ]
    });

    const attempted = await db.AssignmentDocument.findAll({
        where: {
            StudentId: req.body.studentid
        },
        include: [{
            model: db.Assignment,

            include: [db.Subject, db.Chapter]
        }
        ]
    });

    const attemptedids = attempted.map(item => item.AssignmentId).flat();
    const allAssignments = subject.map(item => item.Assignments).flat();
    const result = allAssignments.filter(item => !(attemptedids.includes(item.id)));
    res.send({ attempted: attempted, notAttempted: result });
});
//----------------------------------------------------------------------------

router.post("/getAssignmentStatusbyChapters",requireSignin, async (req, res) => {


    const student = await db.Student.findOne({
        where: {
            id: req.body.studentid
        }
    })

    if (!student) {
        return res.status(403).json({ message: "Student not found" });
    }

    const branch = await db.Branch.findOne({
        where: {
            id: req.body.branch
        }
    })
    if (branch.Status == 0) {
        return res.status(403).json({ message: "Branch deactivated" });
    }


    const Assignments = await db.Assignment.findAll({
        where: {
            ChapterId: req.body.chapterid,
            Branch: req.body.branch
        },
        include: [
            db.Subject, db.Chapter
        ]
    });


    const attempted = await db.AssignmentDocument.findAll({
        where: {
            StudentId: req.body.studentid
        },
        include: [
            {
                model: db.Assignment,
                where: {
                    ChapterId: req.body.chapterid
                }
            }
        ]
    });
    const attemptedids = attempted.map(item => item.AssignmentId).flat();
    const result = Assignments.filter(item => !(attemptedids.includes(item.id)));
    res.send({ attempted: attempted, notAttempted: result });
});
module.exports = router;