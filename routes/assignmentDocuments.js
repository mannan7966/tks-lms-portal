const express = require("express");
const router = express.Router();
const db = require("../models");
const multer = require("multer");
const { requireSignin } = require("../auth/passport");


// const upload = multer({ dest: 'public/' });
// router.post("/upload", upload.single("file"), function (req, res, next) {
//     console.log(req.file);
//     db.AssignmentDocument.create({
//         Link: req.file.path,
//         StudentId: req.body.studentid,
//         AssignmentId: req.body.assignmentid,
//     }).then(record => res.send(record));
// })


//register assignment 
router.post("/post", async(req, res) => {

    const student = await db.Student.findOne({
        where: {
            id: req.body.studentid
        }
    })

    if(!student){
        return res.status(403).json({ message: "Student not found" });
    }

    const branch = await db.Branch.findOne({
        where: {
            id: req.body.branch
        }
    })
    if (branch.Status == 0) {
        return res.status(403).json({ message: "Branch deactivated" });
    }

    db.AssignmentDocument.create({
        Answer: req.body.answer,
        StudentId: req.body.studentid,
        AssignmentId: req.body.assignmentid,
        QuestionId:req.body.questionid,
        Checked:req.body.checked
    }).then(record => res.send(record));
});



//get everything of assignments by assignmentid
router.get("/getAssignment/:id",requireSignin, (req, res) => {

    db.AssignmentDocument.findAll({
        where: {
            AssignmentId: req.params.id
        },
        include: [db.Student,db.Question]
    }).then(record => res.send(record)
    )
});


//get everything of assignments by studentid
router.get("/getSAssignment/:studentid/:assignmentid",requireSignin, (req, res) => {

    db.AssignmentDocument.findAll({
        where: {
            StudentId: req.params.studentid,
            AssignmentId:req.params.assignmentid
        },
        include: [db.Question,db.Assignment]
    }).then(record => res.send(record)
    )
});



//get everything of assignment

router.get("/getEverything/:id",requireSignin, (req, res) => {

    db.AssignmentDocument.findAll({
        where: {
            StudentId: req.params.id
        },
        include: [
            {
                model: db.Assignment,
                include: [
                    {
                        model: db.Chapter,
                    },
                    {
                        model: db.Subject,
                    }
                ]
            }
        ]
    }).then(record => res.send(record)
    )
});


router.put("/addMarks",requireSignin, (req, res) => {
    db.AssignmentDocument.update(
        {
            Marks:req.body.marks,
            //TotalMarks:req.body.totalmarks,
            Checked :"1"
        },
        {
            where: {
                StudentId: req.body.studentid,
                AssignmentId:req.body.assignmentid,
                QuestionId: req.body.questionid
            }
        }).then((date) => res.send(date));
});

router.put("/changeChecked",requireSignin, (req, res) => {
    db.AssignmentDocument.update(
        {
            Checked :"0"
        },
        {
            where: {
                StudentId: req.body.studentid,
                AssignmentId:req.body.assignmentid,
                QuestionId: req.body.questionid
            }
        }).then((date) => res.send(date));
});

module.exports = router;