const express = require("express");
const router = express.Router();
const db = require("../models");

//register video of the chapter
router.post("/registerVideo", (req, res) => {
    db.Video.create({
        Link: req.body.link,
        ChapterId: req.body.chapterid,
        SubjectId: req.body.subjectid,
    }).then(video => res.send(video));
});


//delete video 
router.delete("/delete/video/:id", (req, res) => {
    db.Video.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));

});

//get videos of chapter
router.get("/video/:chapterid", (req, res) => {

    db.Video.findAll({
        where: {
            ChapterId: req.params.chapterid
        },
    }).then(video => res.send(video));
}
);

//update Link of video
router.put("/updateLink", (req, res) => {
    db.Video.update(
        {
            Link: req.body.link
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});


module.exports = router;