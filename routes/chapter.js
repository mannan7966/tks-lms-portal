const express=require("express");
const router = express.Router();
const db=require("../models");
const quiz = require("../models/quiz");
const { requireSignin } = require("../auth/passport");


//Register a new chapter
router.post("/registerChapter", (req,res)=>{
     db.Chapter.create({
         Chapter_No:req.body.chapterno,
         Name:req.body.name,
         SubjectId:req.body.subjectid,
     }).then(chapter => res.send(chapter));
 });

 
 //get videos of the chapter
 router.get("/find/:chapterid", (req, res) => {

    db.Chapter.findOne({
        where: {
            id: req.params.chapterid
        },
    }).then(chapter => res.send(chapter));
}
);


 //get videos of the chapter
 router.get("/video/:chapterid", (req, res) => {

    db.Video.findOne({
        attributes: [
            'id',
            'Link',
        ],
        where: {
            ChapterId: req.params.chapterid
        },
    }).then(video => res.send(video));
}
);

//get pdf of the chapter
router.get("/pdf/:chapterid", (req, res) => {

    db.PDF.findOne({
        attributes: [
            'id',
            'Link',
        ],
        where: {
            ChapterId: req.params.chapterid
        },
    }).then(pdf => res.send(pdf));
}
);


//get assignment of the chapter
router.post("/assignment", (req, res) => {

    db.Assignment.findAll({
        where: {
            ChapterId: req.body.chapterid,
            Branch: req.body.branch,
        },
    }).then(assignment => res.send(assignment));
}
);

//get quiz of the chapter
router.post("/quiz", async (req, res) => {

    db.Quiz.findAll({
        where: {
            ChapterId: req.body.chapterid,
            Branch: req.body.branch,
        },
    }).then(quiz => res.send(quiz));
}
);




//delete chapter

router.delete("/delete/chapter/:id", (req, res) => {
    db.Chapter.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));

});


//update name of Chapter
router.put("/updateName", (req, res) => {
    db.Chapter.update(
        {
            Name: req.body.name
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});

//update number of Chapter
router.put("/updateNumber", (req, res) => {
    db.Chapter.update(
        {
            Chapter_No: req.body.number
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});
 module.exports=router;