const express = require("express");
const router = express.Router();
const db = require("../models");
const { requireSignin } = require("../auth/passport");


//register a new pdf
router.post("/registerPDF", (req, res) => {
    db.PDF.create({
        Link: req.body.link,
        ChapterId: req.body.chapterid,
    }).then(pdf => res.send(pdf));
});

//delete pdf
router.delete("/delete/pdf/:id",requireSignin, (req, res) => {
    db.PDF.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));

});

//get pdf of chapter
router.get("/pdf/:chapterid",requireSignin, (req, res) => {

    db.PDF.findAll({
        where: {
            ChapterId: req.params.chapterid
        },
    }).then(video => res.send(video));
}
);

//update Link of pdf
router.put("/updateLink",requireSignin, (req, res) => {
    db.PDF.update(
        {
            Link: req.body.link
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});




module.exports = router;