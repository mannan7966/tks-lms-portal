const express = require("express");
const router = express.Router();
const db = require("../models");
const { requireSignin } = require("../auth/passport");


//register a new teacher
router.post("/register", async(req, res) => {
    const code = await db.Teacher.findOne({
        limit: 1,
        order: [['createdAt', 'DESC']]
    })

    const branch = await db.Branch.findOne({
        where: {
            id: req.body.branchid
        }
    })
    var alias =branch.Code;
    alias = alias.substring(4, 7);
    console.log(alias);

    if (!code) {
        console.log("table empty");
        const tempId="TKS-"+alias+"-T-0001"
        db.Teacher.create({
            Teacher_id: tempId,
            Name: req.body.name,
            Address: req.body.address,
            UserId: req.body.id,
            BranchId: req.body.branchid,
        }).then(Teacher => res.send(Teacher)).catch(function (err) {
            res.send("Failed");
        }
        );
    }
    else{
        const temp1=code.id+1+10000;
        const temp2=temp1.toString();
        const temp = "TKS-"+alias+"-T-" + temp2.substring(1);
        console.log(temp);
        db.Teacher.create({
            Teacher_id: temp,
            Name: req.body.name,
            Address: req.body.address,
            UserId: req.body.id,
            BranchId: req.body.branchid,
        }).then(Teacher => res.send(Teacher)).catch(function (err) {
            res.send("Failed");
        }
        );
    }

    
});



//get teacher by id
router.get("/teacher/:rollno",requireSignin, (req, res) => {
    db.Teacher.findAll({
        where: {
            Teacher_id: req.params.rollno
        },
        include: [db.User, db.Branch]
    }).then(user => res.send(user));
});

//get teacher by id
router.get("/findTeacher/:rollno",requireSignin, (req, res) => {
    db.Teacher.findOne({
        where: {
            Teacher_id: req.params.rollno
        },
        include: [db.User, db.Branch]
    }).then(user => res.send(user));
});

//get single teacher element by id
router.get("/findSingle/:rollno",requireSignin, (req, res) => {
    db.User.findAll({
        where: {
            id: req.params.rollno
        },
    }).then(user => res.send(user));
});

//delete teacher
router.delete("/deleteTeacher/:teacherid",requireSignin, (req, res) => {
    db.Teacher.destroy({
        where: {
            Teacher_id: req.params.teacherid
        }
    }).then(() => res.send({ message: "success" }));


});

//delete user
router.delete("/deleteUser/:id",requireSignin, (req, res) => {
    db.User.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));
});




//update API's

//update Name
router.put("/updateName",requireSignin, (req, res) => {
    db.Teacher.update(
        {
            Name: req.body.name
        },
        {
            where: {
                Teacher_id: req.body.teacherid
            }
        }).then(() => res.send("success"));
});


//update Address
router.put("/updateAddress",requireSignin, (req, res) => {
    db.Teacher.update(
        {
            Address: req.body.address
        },
        {
            where: {
                Teacher_id: req.body.teacherid
            }
        }).then(() => res.send("success"));
});


//update Password

router.put("/updatePassword",requireSignin, (req, res) => {
    db.User.update(
        {
            Password: req.body.password
        },
        {
            where: {
                id: req.body.userid
            }
        }).then(() => res.send("success"));
});


//update Guardian

router.put("/updateGuardian",requireSignin, (req, res) => {
    db.User.update(
        {
            Guardian: req.body.guardian
        },
        {
            where: {
                id: req.body.userid
            }
        }).then(() => res.send("success"));
});

//update phone number

router.put("/updateNumber",requireSignin, (req, res) => {
    db.User.update(
        {
            Phone_1: req.body.phone_1
        },
        {
            where: {
                id: req.body.userid
            }
        }).then(() => res.send("success"));
});



//update Backup number

router.put("/updateBackup",requireSignin, (req, res) => {
    db.User.update(
        {
            Phone_2: req.body.phone_2
        },
        {
            where: {
                id: req.body.userid
            }
        }).then(() => res.send("success"));
});


module.exports = router;