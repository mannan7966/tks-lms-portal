const express = require("express");
const router = express.Router();
const db = require("../models");
const { requireSignin } = require("../auth/passport");


//post marks of the student
router.post("/postMarks",requireSignin, async(req, res) => {

    const isRecordFound = await db.Progress_Report.findOne({
        where: {
            StudentId: req.body.studentid,
            SubjectId: req.body.subjectid,
            Term: req.body.term
        }
    })

    if(isRecordFound){
        db.Progress_Report.update(
            {
                Total_Marks: req.body.totalmarks,
                Marks: req.body.marks,
            },
            {
                where: {
                    StudentId: req.body.studentid,
                    SubjectId: req.body.subjectid,
                    Term: req.body.term
                }
            }).then((report) => res.send(report));
    }
    else {
        db.Progress_Report.create({
            Total_Marks: req.body.totalmarks,
            Marks: req.body.marks,
            StudentId: req.body.studentid,
            SubjectId: req.body.subjectid,
            Term: req.body.term
        }).then(report => res.send(report));
    }    
});


//delete progress report of the student (because of student)
router.delete("/delete/progressReport/:id",requireSignin, (req, res) => {
    db.Progress_Report.destroy({
        where: {
            StudentId: req.params.id
        }
    }).then(() => res.send({ message: "success" }));

});


//delete progress report of the student (because of subject)
router.delete("/delete/progressReport/subjects/:id",requireSignin, (req, res) => {
    db.Progress_Report.destroy({
        where: {
            SubjectId: req.params.id
        }
    }).then(() => res.send({ message: "success" }));

});



// //get report card by term
// router.get("/question/:id", (req, res) => {

//     db.Question.findOne({
//         where: {
//             id: req.params.id
//         },
//     }).then(question => res.send(question));
// }
// );
module.exports = router;