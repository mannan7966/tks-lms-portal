const express = require("express");
const router = express.Router();
const db = require("../models");
const {requireSignin} = require("../auth/passport");


//Register a new quiz
router.post("/registerQuiz", (req, res) => {
    db.Quiz.create({
        Start_Time: req.body.starttime,
        End_Time: req.body.endtime,
        ChapterId: req.body.chapterid,
        SubjectId: req.body.subjectid,
        TeacherId: req.body.teacherid,
        Branch: req.body.branch,
    }).then(quiz => res.send(quiz));
});

//get quiz question
router.get("/question/:quizid", requireSignin, (req, res) => {

        db.Question.findAll({
            where: {
                quizId: req.params.quizid
            },
        }).then(question => res.send(question));
    }
);

//delete quiz
router.delete("/delete/quiz/:id", requireSignin, (req, res) => {
    db.Quiz.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({message: "success"}));

});

//update start time
router.put("/updateStart", requireSignin, (req, res) => {
    db.Quiz.update(
        {
            Start_Time: req.body.starttime,
        },
        {
            where: {
                id: req.body.id
            }
        }).then((date) => res.send(date));
});

//update end time
router.put("/updateEnd", requireSignin, (req, res) => {
    db.Quiz.update(
        {
            End_Time: req.body.endtime,
        },
        {
            where: {
                id: req.body.id
            }
        }).then((date) => res.send(date));
});


// get quizes by teachers

router.get("/getEverything/:id", requireSignin, (req, res) => {

    db.Quiz.findAll({
        where: {
            TeacherId: req.params.id
        },
        include: [
            {
                model: db.Chapter,
            },
            {
                model: db.Subject,
            }
        ]
    }).then(record => res.send(record)
    )
});

// get quiz by id

router.get("/getQuiz/:id", requireSignin, (req, res) => {

    db.Quiz.findOne({
        where: {
            id: req.params.id
        },
        include: [
            db.Chapter, db.Subject
        ]
    }).then(Quiz => res.send(Quiz)
    )
});


router.post("/getQuizStatusbySubjects", requireSignin, async (req, res) => {

    const student = await db.Student.findOne({
        where: {
            id: req.body.studentid
        }
    })

    if (!student) {
        return res.status(403).json({message: "Student not found"});
    }

    const branch = await db.Branch.findOne({
        where: {
            id: req.body.branch
        }
    })
    if (branch.Status == 0) {
        return res.status(403).json({message: "Branch deactivated"});
    }

    const subject = await db.Subject.findAll({
        where: {
            Class: req.body.class
        },
        include: [{
            model: db.Quiz,
            where: {
                Branch: req.body.branch
            },
            include: [db.Subject, db.Chapter]
        }
        ]
    });

    const attempted = await db.Marks.findAll({
        where: {
            StudentId: req.body.studentid
        },
        include: [{
            model: db.Quiz,
            include: [db.Subject, db.Chapter]
        }
        ]
    });

    const attemptedids = attempted.map(item => item.QuizId);
    const allquizes = subject.map(item => item.Quizzes).flat();
    const result = allquizes.filter(item => !(attemptedids.includes(item.id)));
    res.send({attempted: attempted, notAttempted: result});
    //res.send(attempted);
});
//-----------------------------------------------------------------
router.post("/getQuizStatusbyChapters", requireSignin, async (req, res) => {

    const student = await db.Student.findOne({
        where: {
            id: req.body.studentid
        }
    })

    if (!student) {
        return res.status(403).json({message: "Student not found"});
    }

    const branch = await db.Branch.findOne({
        where: {
            id: req.body.branch
        }
    })
    if (branch.Status == 0) {
        return res.status(403).json({message: "Branch deactivated"});
    }

    const Quizzes = await db.Quiz.findAll({
        where: {
            ChapterId: req.body.chapterid
        },
        include: [
            db.Subject, db.Chapter
        ]
    });


    const attempted = await db.Marks.findAll({
        where: {
            StudentId: req.body.studentid
        },
        include: [
            {
                model: db.Quiz,
                where: {
                    ChapterId: req.body.chapterid,
                    Branch: req.body.branch
                }
            }
        ]
    });
    const attemptedids = attempted.map(item => item.QuizId).flat();
    const result = Quizzes.filter(item => !(attemptedids.includes(item.id)));
    res.send({attempted: attempted, notAttempted: result});
});

module.exports = router;