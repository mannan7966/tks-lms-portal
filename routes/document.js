const express = require("express");
const router = express.Router();
const db = require("../models");
const multer = require("multer");
const { requireSignin } = require("../auth/passport");


const upload = multer({ dest: 'public/' });
router.post("/upload",requireSignin, upload.single("file"), function (req, res, next) {
    console.log(req.file);
    console.log(req.body.name);
    // res.send(req.file.path);

    db.Document.create({
        Title: req.body.name,
        Link: req.file.path
    }).then(document => res.send(document))
})



//get all Documents
router.get("/all",requireSignin, (req, res) => {
    // res.header("Access-Control-Allow-Origin", "*");
     db.Document.findAll(
    ).then(documents => res.send(documents));
});

//delete Document 
router.delete("/delete/document/:id",requireSignin, (req, res) => {
    db.Document.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));

});



module.exports = router;