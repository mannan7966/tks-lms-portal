const express = require("express");
const router = express.Router();
const db = require("../models");
const multer = require("multer");
const { requireSignin } = require("../auth/passport");

const upload = multer({ dest: 'public/' });

router.post("/register", upload.single("file"), function (req, res, next) {
    console.log(req.file);
    console.log(req.body);

    db.Subject.create({
        Name: req.body.name,
        Class: req.body.class,
        PDF: req.file.path,
        Marks: req.body.marks,
    }).then(subject => res.send(subject));
})


//get all subjects
router.get("/findAll",requireSignin, (req, res) => {
    // res.header("Access-Control-Allow-Origin", "*");
    db.Subject.findAll().then(subject => res.send(subject));
});

//find Subject
router.get("/find/:id", (req, res) => {
    db.Subject.findOne({
        where: {
            id: req.params.id
        },
    }).then(subject => res.send(subject));
});


//get chapters of the subject
router.get("/chapters/:subjectid",requireSignin, (req, res) => {

    db.Chapter.findAll({
        where: {
            SubjectId: req.params.subjectid
        },
    }).then(chapter => res.send(chapter));
}
);

//get chapter with subject by chapterid
router.get("/chapter/:chapterid",requireSignin, (req, res) => {

    db.Chapter.findOne({
        where: {
            id: req.params.chapterid
        },
        include: [db.Subject]
    }).then(chapter => res.send(chapter));
}
);



//get subjects by class
router.get("/subjects/:class",requireSignin, (req, res) => {

    db.Subject.findAll({
        where: {
            Class: req.params.class
        },
    }).then(chapter => res.send(chapter));
}
);


//delete subject
router.delete("/delete/subject/:id",requireSignin, (req, res) => {

    db.Progress_Report.destroy({
        where: {
            SubjectId: req.params.id
        }
    });

    db.Subject.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));
});


//update name of subject
router.put("/updateName",requireSignin, (req, res) => {
    db.Subject.update(
        {
            Name: req.body.name
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});


//update Marks of subject
router.put("/updateMarks",requireSignin, (req, res) => {

    db.Progress_Report.update(
        {
            Marks: 0,
        },
        {
            where: {
                SubjectId: req.body.id
            }
        });

    db.Subject.update(
        {
            Marks: req.body.marks
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});



//update class of subject
router.put("/updateClass",requireSignin, (req, res) => {
    db.Subject.update(
        {
            Class: req.body.class
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});


//update PDF of subject
// router.put("/updatePdf", (req, res) => {
//     db.Subject.update(
//         {
//             PDF: req.body.pdf
//         },
//         {
//             where: {
//                 id: req.body.id
//             }
//         }).then(() => res.send("success"));
// });


router.post("/updatePdf",requireSignin, upload.single("file"), function (req, res, next) {
    // console.log(req.file);
    // console.log(req.body.id);
    db.Subject.update(
        {
            PDF: req.file.path,
            StudentId: req.body.studentid,
            AssignmentId: req.body.assignmentid,
        },

        {
            where: {
                id: req.body.id
            }

        }
    ).then(record => res.send(record));
})



module.exports = router;