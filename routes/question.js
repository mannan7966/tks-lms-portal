const express = require("express");
const router = express.Router();
const db = require("../models");
const { requireSignin } = require("../auth/passport");


//post question for assignment
router.post("/postAQuestion",requireSignin, (req, res) => {
    db.Question.create({
        Statement: req.body.statement,
        A: req.body.a,
        B: req.body.b,
        C: req.body.c,
        D: req.body.d,
        Correct: req.body.correct,
        AssignmentId: req.body.assignmentid
    }).then(question => res.send(question));
});

//post question for quiz
router.post("/postQQuestion",requireSignin, (req, res) => {
    db.Question.create({
        Statement: req.body.statement,
        A: req.body.a,
        B: req.body.b,
        C: req.body.c,
        D: req.body.d,
        Correct: req.body.correct,
        QuizId: req.body.quizid
    }).then(question => res.send(question));
});


//get quiz question
router.get("/question/:id",requireSignin, (req, res) => {

    db.Question.findOne({
        where: {
            id: req.params.id
        },
    }).then(question => res.send(question));
}
);



//delete question
router.delete("/delete/question/:id",requireSignin, (req, res) => {
    db.Question.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));

});


//update statement
router.put("/updateStatement",requireSignin, (req, res) => {
    db.Question.update(
        {
            Statement: req.body.statement,
        },
        {
            where: {
                id: req.body.id
            }
        }).then((date) => res.send(date));
});

//update A
router.put("/updateA",requireSignin, (req, res) => {
    db.Question.update(
        {
            A: req.body.a,
        },
        {
            where: {
                id: req.body.id
            }
        }).then((date) => res.send(date));
});

//update B
router.put("/updateB",requireSignin, (req, res) => {
    db.Question.update(
        {
            B: req.body.b,
        },
        {
            where: {
                id: req.body.id
            }
        }).then((date) => res.send(date));
});

//update C
router.put("/updateC",requireSignin, (req, res) => {
    db.Question.update(
        {
            C: req.body.c,
        },
        {
            where: {
                id: req.body.id
            }
        }).then((date) => res.send(date));
});

//update D
router.put("/updateD",requireSignin, (req, res) => {
    db.Question.update(
        {
            D: req.body.d,
        },
        {
            where: {
                id: req.body.id
            }
        }).then((date) => res.send(date));
});

//update correct
router.put("/updateCorrect",requireSignin, (req, res) => {
    db.Question.update(
        {
            Correct: req.body.correct,
        },
        {
            where: {
                id: req.body.id
            }
        }).then((date) => res.send(date));
});

module.exports = router;