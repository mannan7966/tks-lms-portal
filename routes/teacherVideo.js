const express = require("express");
const router = express.Router();
const db = require("../models");
const { requireSignin } = require("../auth/passport");


//register video
router.post("/register", (req, res) => {
    db.Teacher_Video.create({
        Title: req.body.title,
        Link: req.body.link,
    }).then(video => res.send(video));
});

//get video
router.get("/video/:id",requireSignin, (req, res) => {

    db.Teacher_Video.findOne({
        where: {
            id: req.params.id
        },
    }).then(video => res.send(video));
}
);

//get all videos
router.get("/findAll",requireSignin, (req, res) => {
    db.Teacher_Video.findAll().then(video => res.send(video));
});



//delete video
router.delete("/delete/video/:id",requireSignin, (req, res) => {
    db.Teacher_Video.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));
});

//update title of video
router.put("/updateTitle",requireSignin, (req, res) => {
    db.Teacher_Video.update(
        {
            Title: req.body.title
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});

//update link of video
router.put("/updateLink",requireSignin, (req, res) => {
    db.Teacher_Video.update(
        {
            Link: req.body.link
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});

module.exports = router;