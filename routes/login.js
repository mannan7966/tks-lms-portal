require("dotenv").config();
const express = require("express");
const bcrypt = require("bcrypt");
const router = express.Router();
const db = require("../models");
var User = require('../models').User;
const jwt = require("jsonwebtoken");


//student login
router.post("/login", async (req, res) => {
    console.log(req.body)
    const email = req.body.email;
    const password = req.body.password;
    const userWithEmail = await User.findOne({
        where: {
            Email: email
        }
    }).catch((err) => {
        console.log("Error : ", err);
    });
    if (!userWithEmail) {
        return res.status(403).json({ message: "Email does not exist!" });
    }
    if (userWithEmail.Password != password) {
        return res.status(403).json({ message: "password does not matched!" });
    }
    const jwtToken = jwt.sign({ id: userWithEmail.id, email: userWithEmail.Email }, process.env.JWT_KEY, {expiresIn : '24h'});


    const branch= await db.Student.findOne({
        where: {
            UserId: userWithEmail.id
        },
        include: [db.User, db.Branch]
    });

    if(branch.Branch.Status==0){
        return res.status(403).json({ message: "Branch deactivated" });
    }
    // res.send(branch);

    db.Student.findOne({
        where: {
            UserId: userWithEmail.id
        },
        include: [db.User, db.Branch]
    }).then(user => {
        res.send({ id: userWithEmail.id, type: userWithEmail.Type, token: jwtToken, userInfo: user });
    }
    );


});


//teacher login
router.post("/Teacherlogin", async (req, res) => {
    console.log(req.body)
    const email = req.body.email;
    const password = req.body.password;
    const userWithEmail = await User.findOne({
        where: {
            Email: email
        }
    }).catch((err) => {
        console.log("Error : ", err);
    });
    if (!userWithEmail) {
        return res.status(403).json({ message: "Email does not exist!" });
    }
    
    if (userWithEmail.Password != password) {
        return res.status(403).json({ message: "password does not match!" });
    }
    console.log(userWithEmail.Type);
    
    const jwtToken = jwt.sign({ id: userWithEmail.id, email: userWithEmail.Email }, process.env.JWT_KEY, {expiresIn : '24h'});
    
    const principal = await db.Principal.findOne({
        where: {
            UserId: userWithEmail.id
        }
    });
    if (principal == null) {
        if (userWithEmail.Type == 4) {
            db.Teacher.findOne({
                where: {
                    UserId: userWithEmail.id
                },
            }).then(user => {
                res.send({ id: userWithEmail.id, phone: userWithEmail.Phone_1, type: userWithEmail.Type, pic: userWithEmail.Picture, token: jwtToken, user: user });
            }
            );a
        }
        else if (userWithEmail.Type == 2) {
            const Teacher = await db.Teacher.findOne({
                where: {
                    UserId: userWithEmail.id
                },
            });
            db.Branch.findOne({
                where: {
                    id: Teacher.BranchId
                }
            }).then(user => {
                res.send({ id: userWithEmail.id, phone: userWithEmail.Phone_1, type: userWithEmail.Type, pic: userWithEmail.Picture, token: jwtToken, user: Teacher, branch: user });
            }
            );

        }
        else if (userWithEmail.Type == 1) {
        res.send({type:userWithEmail.Type})
        }

    }
    
    else {
        db.Branch.findOne({
            where: {
                id: principal.BranchId
            }
        }).then(user => {
            res.send({ id: userWithEmail.id, phone: userWithEmail.Phone_1, type: userWithEmail.Type, pic: userWithEmail.Picture, token: jwtToken, branchid: principal.BranchId, branch: user });
        }
        );

    }

});


//authentication
router.post("/authenticate", async (req, res) => {
    //console.log(req.body.Token);
    jwt.verify(req.body.Token, process.env.JWT_KEY, (err, authData) => {
        // if (err) {
        //     res.sendStatus("403");
        // }
        // else {
        res.json({
            message: "Welcome",
            authData
        });
        // }
    });
})


//authentication
router.post("/authenticateUser", async (req, res) => {
    //console.log(req.body.Token);
    const email = req.body.email;
    const userWithEmail = await User.findOne({
        where: {
            Email: email
        }
    }).catch((err) => {
        console.log("Error : ", err);
    });
    if (!userWithEmail) {
        return res.send({ message: "Failed" });
    }
    else{
        res.send({message:"success"});
    }
});


module.exports = router;