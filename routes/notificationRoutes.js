const express = require("express");
const router = express.Router();
const db = require("../models");
const notification = require("../models/notification");
const { requireSignin } = require("../auth/passport");




//get all notifications
router.get("/allByManagementP",requireSignin, (req, res) => {
    db.Notification.findAll({
        where: {
            PostedBy: "4",
            PostedFor: "3"
        }
    }).then(notifications => res.send(notifications));
});

router.get("/allByManagementT",requireSignin, (req, res) => {
    db.Notification.findAll({
        where: {
            PostedBy: "4",
            PostedFor: "2"
        }
    }).then(notifications => res.send(notifications));
});

router.get("/allByManagementS",requireSignin, (req, res) => {
    db.Notification.findAll({
        where: {
            PostedBy: "4",
            PostedFor: "1"
        }
    }).then(notifications => res.send(notifications));
});


// all for students
router.post("/allforstudents",requireSignin, async(req, res) => {

    const student = await db.Student.findOne({
        where: {
            id: req.body.studentid
        }
    })

    if(!student){
        return res.status(403).json({ message: "Student not found" });
    }

    const branch = await db.Branch.findOne({
        where: {
            id: req.body.branch
        }
    })
    if (branch.Status == 0) {
        return res.status(403).json({ message: "Branch deactivated" });
    }

    const principalNotification = await db.Notification.findAll({
        where: {
            PostedBy: "3",
            PostedFor: "1",
            Branch: req.body.branch
        },
    });//.then(notifications => res.send(notifications));
    const managementNotification = await db.Notification.findAll({
        where: {
            PostedBy: "4",
            PostedFor: "1"
        }
    })//.then(notifications => res.send(notifications));
    const notification=principalNotification.concat(managementNotification)
    res.send({Notifications:notification})
});




router.post("/allByPrincipalT",requireSignin, (req, res) => {
    console.log(req.body.branch);
    db.Notification.findAll({
        where: {
            PostedBy: "3",
            PostedFor: "2",
            Branch: req.body.branch
        }
    }).then(notifications => res.send(notifications));
});

router.post("/allByPrincipalS",requireSignin, (req, res) => {
    console.log(req.body.branch);
    db.Notification.findAll({
        where: {
            PostedBy: "3",
            PostedFor: "1",
            Branch: req.body.branch
        }
    }).then(notifications => res.send(notifications));
});

router.get("/allforTByM",requireSignin, (req, res) => {
    db.Notification.findAll({
        where: {
            PostedBy: "4",
            PostedFor: "2"
        }
    }).then(notifications => res.send(notifications));
});




router.post("/allforTByP", (req, res) => {
    db.Notification.findAll({
        where: {
            PostedBy: "3",
            PostedFor: "2",
            Branch: req.body.branch
        }
    }).then(notifications => res.send(notifications));
});

router.get("/allforSByM", (req, res) => {
    db.Notification.findAll({
        where: {
            PostedBy: "4",
            PostedFor: "1"
        }
    }).then(notifications => res.send(notifications));
});

router.get("/allforSByP", (req, res) => {
    db.Notification.findAll({
        where: {
            PostedBy: "3",
            PostedFor: "1",
            Branch: req.body.branch
        }
    }).then(notifications => res.send(notifications));
});

//get all notifications
router.get("/all", (req, res) => {
    db.Notification.findAll().then(notifications => res.send(notifications));
});

router.get("/allP", (req, res) => {
    db.Notification.findAll(
        {
            where: {
                PostedFor: "3"
            }
        }
    ).then(notifications => res.send(notifications));
});



//get a notification by id
router.get("/find/:id", (req, res) => {
    db.Notification.findAll({
        where: {
            id: req.params.id
        }
    }).then(notification => res.send(notification));
});



//post a new notification by management
router.post("/newMtoP", (req, res) => {
    db.Notification.create({
        Description: req.body.description,
        Title: req.body.title,
        PostedFor: req.body.postedfor,
        PostedBy: req.body.postedby
    }).then(submitedNotification => res.send(submitedNotification));
});

//post a new notification by Principal 
router.post("/newPtoT", (req, res) => {
    db.Notification.create({
        Description: req.body.description,
        Title: req.body.title,
        PostedFor: req.body.postedfor,
        PostedBy: req.body.postedby,
        Branch: req.body.branch
    }).then(submitedNotification => res.send(submitedNotification));
});





//to delete a notification
router.delete("/delete/:id", (req, res) => {
    db.Notification.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));
});

//to update a notification
router.put("/edit", (req, res) => {
    db.Notification.update(
        {
            Description: req.body.text
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});

module.exports = router;