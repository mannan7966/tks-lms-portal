const express = require("express");
const router = express.Router();
const db = require("../models");
var sequelize = require("sequelize");
const { requireSignin } = require("../auth/passport");



//register a new student
router.post("/register", async (req, res) => {

    const code = await db.Student.findOne({
        limit: 1,
        order: [['createdAt', 'DESC']]
    })

    const branch = await db.Branch.findOne({
        where: {
            id: req.body.branchid
        }
    })
    var alias = branch.Code;
    alias = alias.substring(4, 7);
    console.log(alias);

    if (!code) {
        console.log("table empty");
        const tempId = "TKS-" + alias + "-S-0001"
        db.Student.create({
            Roll_No: tempId,
            Name: req.body.name,
            Address: req.body.address,
            Class: req.body.class,
            UserId: req.body.id,
            BranchId: req.body.branchid,
        }).then(student => res.send(student)).catch(function (err) {
            res.send("Failed");
        }
        );
    }

    else {
        const temp1 = code.id + 1 + 10000;
        const temp2 = temp1.toString();
        const temp = "TKS-" + alias + "-S-" + temp2.substring(1);
        console.log(temp);
        db.Student.create({
            Roll_No: temp,
            Name: req.body.name,
            Address: req.body.address,
            Class: req.body.class,
            UserId: req.body.id,
            BranchId: req.body.branchid,
        }).then(student => res.send(student)).catch(function (err) {
            res.send("Failed");
        }
        );
    }

});


//delete student
router.delete("/deleteStudent/:rollno",requireSignin, (req, res) => {
    db.Student.destroy({
        where: {
            Roll_No: req.params.rollno
        }
    }).then(() => res.send({ message: "success" }));


});


//register subjects
router.post("/registerSubject", (req, res) => {
    db.Student_Subject.create({
        StudentId: req.body.studentid,
        SubjectId: req.body.subjectid,
    }).then(student => res.send(student));
});


//get student by rollno
router.get("/student/:rollno",requireSignin, (req, res) => {

    db.Student.findAll({
        where: {
            Roll_No: req.params.rollno
        },
        include: [db.User, db.Branch]
    }).then(user => res.send(user));
});

//get student by rollno
router.get("/findStudent/:rollno",requireSignin, (req, res) => {

    db.Student.findOne({
        where: {
            Roll_No: req.params.rollno
        },
        include: [db.User, db.Branch]
    }).then(user => res.send(user));
});


//get student by rollno
router.get("/students/:rollno",requireSignin, (req, res) => {

    db.Student.findOne({
        where: {
            Roll_No: req.params.rollno
        },
    }).then(user => res.send(user));
});


//get progress report
router.get("/progressReport/:id/:branch",requireSignin, async (req, res) => {

    const student = await db.Student.findOne({
        where: {
            id: req.params.id
        }
    })

    if (!student) {
        return res.status(403).json({ message: "Student not found" });
    }

    const branch = await db.Branch.findOne({
        where: {
            id: req.params.branch
        }
    })
    if (branch.Status == 0) {
        return res.status(403).json({ message: "Branch deactivated" });
    }


    const firstTerm= await   db.Progress_Report.findAll({
        where: {
            StudentId: req.params.id,
            Term:"1"
        },
        include: [db.Subject]
    })

    const totalFirst = await  db.Progress_Report.findAll({
        where: {
            StudentId: req.params.id,
            Term:"1"
        },
        attributes: [
          [sequelize.fn('sum', sequelize.col('Marks')), 'Total_Obtained'],
        ],
      });
      const obtainedFirst = totalFirst;
      var First;
      if(obtainedFirst[0].dataValues['Total_Obtained']==null){
        First={
           Total_Obtained:"0",
            Subjects:firstTerm
        }
      }
      else{
        First={
            ...obtainedFirst[0].dataValues,
            Subjects:firstTerm
        }
      }
//-----------------------------------------------------
    const secondTerm= await   db.Progress_Report.findAll({
        where: {
            StudentId: req.params.id,
            Term:"2"
        },
        include: [db.Subject]
    })
    const totalSecond = await  db.Progress_Report.findAll({
        where: {
            StudentId: req.params.id,
            Term:"2"
        },
        attributes: [
          [sequelize.fn('sum', sequelize.col('Marks')), 'Total_Obtained'],
        ],
      });
      const obtainedSecond = totalSecond;
      var Second;
      if(obtainedSecond[0].dataValues['Total_Obtained']==null){
        Second={
           Total_Obtained:"0",
           Subjects:secondTerm
        }
      }
      else{
        Second={
            ...obtainedSecond[0].dataValues,
            Subjects:secondTerm
        }
      }
//-------------------------------------------------------
     
    const thirdTerm= await   db.Progress_Report.findAll({
        where: {
            StudentId: req.params.id,
            Term:"3"
        },
        include: [db.Subject]
    })

    const totalThird = await  db.Progress_Report.findAll({
        where: {
            StudentId: req.params.id,
            Term:"3"
        },
        attributes: [
          [sequelize.fn('sum', sequelize.col('Marks')), 'Total_Obtained'],
        ],
      });

      const obtainedThird = totalThird;
      var Third;
      if(obtainedThird[0].dataValues['Total_Obtained']==null){
        Third={
           Total_Obtained:"0",
           Subjects:thirdTerm
        }
      }
      else{
        Third={
            ...obtainedThird[0].dataValues,
          Subjects:thirdTerm
        }
      }
      res.send({first:First,second:Second,third:Third});
    //   res.send({first:First,second:Second,third:Third,fourth:Fourth});
});

//get subjects
router.get("/subjects/:id",requireSignin, (req, res) => {

    db.Student.findOne({
        attributes: [
            'id',
            'Roll_no'
        ],
        where: {
            id: req.params.id
        },
        include: [{
            model: db.Student_Subject,
            attributes: {
                exclude: ["id", "createdAt", "updatedAt", "StudentId"]
            },
            include: {
                model: db.Subject,
            }
        },

        ]
    }).then(user => res.send(user));
}
);

//get subjects by class---------------------
router.get("/getSubjects/:class/:branch/:studentid",requireSignin, async (req, res) => {

    const student = await db.Student.findOne({
        where: {
            id: req.params.studentid
        }
    })
    if (!student) {
        return res.status(403).json({ message: "Student not found" });
    }

    const branch = await db.Branch.findOne({
        where: {
            id: req.params.branch
        }
    })
    if (branch.Status == 0) {
        return res.status(403).json({ message: "Branch deactivated" });
    }



    db.Subject.findAll({
        where: {
            Class: req.params.class
        },
        include: [{
            model: db.Chapter,
            include: [
                {
                    model: db.Assignment,
                    where: {
                        Branch: req.params.branch,
                    },
                    required: false,
                    include: {
                        model: db.Question
                    }
                },
                {
                    model: db.Quiz,
                    where: {
                        Branch: req.params.branch,
                    },
                    required: false,
                    include: {
                        model: db.Question
                    }
                },
                {
                    model: db.Video
                }

            ]
        }
        ]
    }).then(subjects => res.send(subjects));
});

//get subjects by using rollno
router.get("/subject/:rollno/:term",requireSignin, async (req, res) => {

    const student = await db.Student.findOne({
        where: {
            Roll_No: req.params.rollno
        },
    });

    if (student) {
        db.Subject.findAll({
            where: {
                Class: student.Class
            },
            include: {
                required: false,
                model: db.Progress_Report,
                where: {
                    Term: req.params.term,
                    StudentId:student.id
                }
            }
        }).then(subject => res.send(subject));
    }
    else {
        res.send({ error: "no student with such id exists" })
    }
}
);

//get assignments
router.get("/assignment/:id",requireSignin, async (req, res) => {

    const student = await db.Student.findOne({
        where: {
            id: req.params.id
        },
    });
    if (student) {
        db.Subject.findAll({
            where: {
                Class: student.Class
            },
            include: [
                {
                    model: db.Assignment,
                    include: [db.Question]
                },
            ]
        }).then(assignment => res.send(assignment));
    }
    else {
        res.send({ error: "no student with such id exists" })
    }
}
);




//get quizzez
router.get("/quiz/:id",requireSignin, async (req, res) => {

    const student = await db.Student.findOne({
        where: {
            id: req.params.id
        },
    });
    if (student) {
        db.Subject.findAll({
            where: {
                Class: student.Class
            },
            include: [
                {
                    model: db.Quiz,
                    include: [db.Question]
                },
            ]
        }).then(quiz => res.send(quiz));
    }
    else {
        res.send({ error: "no student with such id exists" })
    }
}
);


//get videos
router.get("/video/:id",requireSignin, async (req, res) => {

    const student = await db.Student.findOne({
        where: {
            id: req.params.id
        },
    });
    if (student) {
        db.Subject.findAll({
            where: {
                Class: student.Class
            },
            include: [db.Video]
        }).then(video => res.send(video));
    }
    else {
        res.send({ error: "no student with such id exists" })
    }
}
);

//update API's

//update Name
router.put("/updateName",requireSignin, (req, res) => {
    db.Student.update(
        {
            Name: req.body.name
        },
        {
            where: {
                Roll_No: req.body.rollno
            }
        }).then(() => res.send("success"));
});


//update Address
router.put("/updateAddress",requireSignin, (req, res) => {
    db.Student.update(
        {
            Address: req.body.address
        },
        {
            where: {
                Roll_No: req.body.rollno
            }
        }).then(() => res.send("success"));
});

//update Class
router.put("/updateClass",requireSignin, (req, res) => {
    db.Student.update(
        {
            Class: req.body.class
        },
        {
            where: {
                Roll_No: req.body.rollno
            }
        }).then(() => res.send("success"));
});
module.exports = router;