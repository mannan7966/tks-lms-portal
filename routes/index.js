const express = require("express");
const userRoutes = require("./userRoutes");
const loginApi = require("./login");


const router = express.Router();
router.use(userRoutes);
router.use(loginApi);

module.exports = router;