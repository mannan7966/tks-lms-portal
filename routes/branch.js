const express = require("express");
const router = express.Router();
const db = require("../models");
var sequelize = require("sequelize");
const { requireSignin } = require("../auth/passport");


//Register a new branch
router.post("/registerBranch", async (req, res) => {

    const code = await db.Branch.findOne({
        limit: 1,
        order: [['createdAt', 'DESC']]
    })
    if (!code) {
        console.log("table empty");
        const tempCode= "TKS-"+req.body.alias+"-0001";
        db.Branch.create({
            //Code: req.body.code,
            Code: tempCode,
            Name: req.body.name,
            Address: req.body.address,
            Status: req.body.status,
        }).then(branch => res.send(branch)).catch(function (err) {
            res.send("Failed");
        }
        );
    }
    else {
        const temp1=code.id+1+10000;
        const temp2=temp1.toString();
        const temp="TKS-"+req.body.alias+"-"+temp2.substring(1);
        console.log(temp);

        db.Branch.create({
            Code: temp,
            Name: req.body.name,
            Address: req.body.address,
            Status: req.body.status,
        }).then(branch => res.send(branch)).catch(function (err) {
            res.send("Failed");
        }
        );;
     }
});


//get all branches
router.get("/all",requireSignin, (req, res) => {
    db.Branch.findAll({
        include: [db.Principal]
    }
    ).then(branch => res.send(branch));
});


//to delete a branch
router.delete("/delete/:id",requireSignin, (req, res) => {
    db.Branch.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));
});

//to get a branch by id
router.get("/get/:id",requireSignin, (req, res) => {
    db.Branch.findOne({
        where: {
            id: req.params.id
        },
        include: [db.Principal]
    }).then(branch => res.send(branch));
});

//to get a branch by code
router.get("/getBranch/:code",requireSignin, (req, res) => {

    // console.log(req.params.code);
    // const code = req.params.code.substring(4);
    // console.log(code);
    // var temp = parseInt(code);
    // var temp2 = 1000 - temp;
    // temp = 1000 - temp2;
    // console.log(temp);
    db.Branch.findOne({
        where: {
            Code: req.params.code
        },
        include: [db.Principal]
    }).then(branch => res.send(branch));
});


//to update a Name
router.put("/updateName",requireSignin, (req, res) => {
    db.Branch.update(
        {
            Name: req.body.name
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});

//to update a Name
router.put("/updateAddress",requireSignin, (req, res) => {
    db.Branch.update(
        {
            Address: req.body.address
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});


//get teachers of branch
router.get("/findTeachers/:id",requireSignin, (req, res) => {
    db.Teacher.findAll({
        where: {
            BranchId: req.params.id
        },
    }).then(user => res.send(user));
});


//get Students of branch
router.get("/findStudents/:id",requireSignin, (req, res) => {
    db.Student.findAll({
        where: {
            BranchId: req.params.id
        },
    }).then(user => res.send(user));
});

//get Students of branch by class
router.get("/findStudentsBC/:id/:class",requireSignin, async (req, res) => {

    const student =await db.Student.findAll({
        where: {
            BranchId: req.params.id,
            Class: req.params.class
        },
    });//.then(user => res.send(user));

    const count = await  db.Student.findAll({
        where: {
            BranchId: req.params.id,
            Class: req.params.class
        },
        attributes: [
          [sequelize.fn('count', sequelize.col('id')), 'Count'],
        ],
      });

      const totalCount = count;
      const students={
          ...totalCount[0].dataValues,
          Student:student
      }
    res.send(students);


});

//get Principals of branch
router.get("/findPrincipals",requireSignin, (req, res) => {
    db.Principal.findAll(
        {
            include: [db.Branch]
        }
    ).then(user => res.send(user));
});

//deactivate
router.put("/deactivate",requireSignin, (req, res) => {
    db.Branch.update(
        {
            Status: "0"
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});

//Activate
router.put("/activate",requireSignin, (req, res) => {
    db.Branch.update(
        {
            Status: "1"
        },
        {
            where: {
                id: req.body.id
            }
        }).then(() => res.send("success"));
});


module.exports = router;