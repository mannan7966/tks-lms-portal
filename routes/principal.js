const express = require("express");
const router = express.Router();
const db = require("../models");
const { requireSignin } = require("../auth/passport");


//register a new principal
router.post("/register", async (req, res) => {

    const code = await db.Principal.findOne({
        limit: 1,
        order: [['createdAt', 'DESC']]
    })
    const branch = await db.Branch.findOne({
        where: {
            id: req.body.branchid
        }
    })
    var alias =branch.Code;
    alias = alias.substring(4, 7);
    console.log(alias);

    if (!code) {
        console.log("table empty");
        const tempId="TKS-"+alias+"-P-0001"
        db.Principal.create({
            Principal_id: tempId,
            Name: req.body.name,
            Address: req.body.address,
            UserId: req.body.id,
            BranchId: req.body.branchid
        }).then(Principal => res.send(Principal)).catch(function (err) {
            res.send("Failed");
        }
        );
    }

    else {
        const temp1 = code.id + 1 + 10000;
        const temp2 = temp1.toString();
        const temp = "TKS-"+alias+"-P-" + temp2.substring(1);
        console.log(temp);
        db.Principal.create({
            Principal_id: temp,
            Name: req.body.name,
            Address: req.body.address,
            UserId: req.body.id,
            BranchId: req.body.branchid
        }).then(Principal => res.send(Principal)).catch(function (err) {
            res.send("Failed");
        }
        );
    }

});



//get principal array by id
router.get("/principal/:rollno",requireSignin, (req, res) => {
    db.Principal.findAll({
        where: {
            Principal_id: req.params.rollno
        },
        include: [db.User, db.Branch]
    }).then(user => res.send(user));
});

//get principal array by id
router.get("/findPrincipal/:rollno",requireSignin, (req, res) => {
    // console.log(req.params.rollno.slice(1));
    db.Principal.findOne({
        where: {
            Principal_id: req.params.rollno.slice(1)
        },
        include: [db.User, db.Branch]
    }).then(user => res.send(user));
});

//get single prinicipal element by id
router.get("/findSingle/:rollno",requireSignin, (req, res) => {
    db.User.findAll({
        where: {
            id: req.params.rollno
        },
    }).then(user => res.send(user));
});

//delete Principal
router.delete("/deletePrincipal/:principalid",requireSignin, (req, res) => {
    db.Principal.destroy({
        where: {
            Principal_id: req.params.principalid
        }
    }).then(() => res.send({ message: "success" }));
});

//delete user
router.delete("/deleteUser/:id",requireSignin, (req, res) => {
    db.User.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));
});




//update API's

//update Name
router.put("/updateName",requireSignin, (req, res) => {
    const principalid = req.body.principalid.slice(1);
    db.Principal.update(
        {
            Name: req.body.name
        },
        {
            where: {
                Principal_id: principalid
            }
        }).then((name) => res.send(name));
});


//update Address
router.put("/updateAddress",requireSignin, (req, res) => {
    const principalid = req.body.principalid.slice(1);
    db.Principal.update(
        {
            Address: req.body.address
        },
        {
            where: {
                Principal_id: principalid
            }
        }).then(() => res.send("success"));
});


//update Password

router.put("/updatePassword",requireSignin, (req, res) => {
    db.User.update(
        {
            Password: req.body.password
        },
        {
            where: {
                id: req.body.userid
            }
        }).then(() => res.send("success"));
});


//update Guardian

router.put("/updateGuardian",requireSignin, (req, res) => {
    db.User.update(
        {
            Guardian: req.body.guardian
        },
        {
            where: {
                id: req.body.userid
            }
        }).then(() => res.send("success"));
});

//update phone number

router.put("/updateNumber",requireSignin, (req, res) => {
    db.User.update(
        {
            Phone_1: req.body.phone_1
        },
        {
            where: {
                id: req.body.userid
            }
        }).then(() => res.send("success"));
});



//update Backup number

router.put("/updateBackup",requireSignin, (req, res) => {
    db.User.update(
        {
            Phone_2: req.body.phone_2
        },
        {
            where: {
                id: req.body.userid
            }
        }).then(() => res.send("success"));
});


module.exports = router;