const express = require("express");
const router = express.Router();
const db = require("../models");
const { requireSignin } = require("../auth/passport");



//register assignment marks
router.post("/postQMarks", async(req, res) => {

    const student = await db.Student.findOne({
        where: {
            id: req.body.studentid
        }
    })

    if(!student){
        return res.status(403).json({ message: "Student not found" });
    }

    const branch = await db.Branch.findOne({
        where: {
            id: req.body.branch
        }
    })
    if (branch.Status == 0) {
        return res.status(403).json({ message: "Branch deactivated" });
    }

    db.Marks.create({
        Marks: req.body.marks,
        TotalMarks: req.body.totalmarks,
        QuizId: req.body.quizid,
        StudentId: req.body.studentid
    }).then(marks => res.send(marks));
});



//get students who attempted quizes
router.get("/getQuiz/:id",requireSignin, (req, res) => {

    db.Marks.findAll({
        where: {
            QuizId: req.params.id
        },
        include: [db.Student]
    }).then(record => res.send(record)
    )
});



//get everything of quiz
router.get("/getEverything/:id",requireSignin, (req, res) => {

    db.Marks.findAll({
        where: {
            StudentId: req.params.id
        },
        include: [
            {
                model: db.Quiz,
                include: [
                    {
                        model: db.Chapter,
                    },
                    {
                        model: db.Subject,
                    }
                ]
            }
        ]
    }).then(record => res.send(record)
    )
});

//delete marks
router.delete("/delete/marks/:id",requireSignin, (req, res) => {
    db.Marks.destroy({
        where: {
            id: req.params.id
        }
    }).then(() => res.send({ message: "success" }));
});


module.exports = router;