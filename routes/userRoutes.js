const express = require("express");
const router = express.Router();
const db = require("../models");
// const bcrypt = require("bcrypt")
const User = require("../models/user");
const { cloudinary } = require("../utils/cloudinary");
const { requireSignin } = require("../auth/passport");
// const bcrypt = require("bcrypt");

// const saltRounds = 10;

//get all users
router.get("/all", requireSignin, (req, res) => {
  db.User.findAll().then((users) => res.send(users));
});

//get a user by id
router.get("/find/:id", requireSignin, (req, res) => {
  db.User.findAll({
    where: {
      id: req.params.id,
    },
    include: [db.Student],
  }).then((user) => res.send(user));
});

//get a aingle user by id
router.get("/findSingle/:id", requireSignin, (req, res) => {
  db.User.findOne({
    where: {
      id: req.params.id,
    },
  }).then((user) => res.send(user));
});



//register a new user
router.post("/register", async (req, res) => {
    console.log(req.body)

  try {
    // const passwordhash = await bcrypt.hash(req.body.password, saltRounds);
    let dataToSave = new db.User({
      Email: req.body.email,
      Type: req.body.type,
      Password: req.body.password,

    //   Picture: result.secure_url,
      Guardian: req.body.guardian,
      Phone_1: req.body.phone_1,
      Phone_2: req.body.phone_2,
    });
      await dataToSave.save();
      res.json({
          status: true,
          message: "User Registred Successfully!"
      })
  } catch (error) {
    res.json({
      status: false,
      message: error.message,
    });
  }
});

//update picture
router.put("/update/picture", requireSignin, async (req, res) => {
  const fileStr = req.body.picture;
  const result = await cloudinary.uploader.upload(fileStr, {
    upload_preset: "ml_default",
  });
  db.User.update(
    {
      Picture: result.secure_url,
    },
    {
      where: {
        id: req.body.userid,
      },
    }
  ).then(() => res.send("success"));
});

//to delete a user
router.delete("/delete/:id", requireSignin, (req, res) => {
  db.User.destroy({
    where: {
      id: req.params.id,
    },
  }).then(() => res.send("success"));
});

//update password
router.put("/edit/password", requireSignin, (req, res) => {
  db.User.update(
    {
      Password: req.body.newPassword,
    },
    {
      where: {
        email: req.body.email,
      },
    }
  ).then(() => res.send("success"));
});

//update Picture
router.put("/edit/picture", requireSignin, (req, res) => {
  db.User.update(
    {
      Picture: req.body.newPicture,
    },
    {
      where: {
        email: req.body.email,
      },
    }
  ).then(() => res.send("success"));
});

//update Guardian
router.put("/edit/guardian", requireSignin, (req, res) => {
  db.User.update(
    {
      Guardian: req.body.newGuardian,
    },
    {
      where: {
        email: req.body.email,
      },
    }
  ).then(() => res.send("success"));
});

//update phone_no_1
router.put("/edit/Phone_1", requireSignin, (req, res) => {
  db.User.update(
    {
      Phone_1: req.body.newPhone_1,
    },
    {
      where: {
        email: req.body.email,
      },
    }
  ).then(() => res.send("success"));
});

//update Phone_2
router.put("/edit/Phone_2", requireSignin, (req, res) => {
  db.User.update(
    {
      Phone_2: req.body.newPicture,
    },
    {
      where: {
        email: req.body.email,
      },
    }
  ).then(() => res.send("success"));
});

module.exports = router;
