const express = require("express");
const router = express.Router();
const db = require("../models");
const { requireSignin } = require("../auth/passport");


//post a new notification by management
router.post("/postNotification",requireSignin, (req, res) => {
    db.TeacherNotification.create({
        Description: req.body.description,
        Title: req.body.title,
        SubjectID:req.body.subjectid,
        ChapterID:req.body.chapterid,
        TeacherID:req.body.teacherid,
        Branch:req.body.branch
    }).then(notification => res.send(notification));
});


module.exports = router;