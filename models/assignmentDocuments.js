module.exports = (sequelize, DataTypes) => {
    const Answer = sequelize.define("AssignmentDocument", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Answer: {
            type: DataTypes.STRING(512),
            allowNull: false
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
        Checked:{
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        Marks:{
            type: DataTypes.INTEGER(11),
            allowNull: true
        }
    });
    return Answer;
};