module.exports = (sequelize, DataTypes) => {
    const PDF = sequelize.define("PDF", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Link: DataTypes.STRING(512),
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return PDF;
};