module.exports = (sequelize, DataTypes) => {
    const Document = sequelize.define("Document", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        Title: {
            type: DataTypes.STRING(25),
            allowNull: false,
        },
        Link: {
            type: DataTypes.TEXT,
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Document;
};