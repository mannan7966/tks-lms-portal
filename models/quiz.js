module.exports = (sequelize, DataTypes) => {
    const Quiz = sequelize.define("Quiz", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Start_Time: DataTypes.STRING(512),
        End_Time: DataTypes.STRING(512),
        Branch: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Quiz;
};