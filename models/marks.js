module.exports = (sequelize, DataTypes) => {
    const Marks = sequelize.define("Marks", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Marks: {
            type: DataTypes.STRING(512),
            allowNull: false
        },
        TotalMarks: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Marks;
};