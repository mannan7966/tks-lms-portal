module.exports = (sequelize, DataTypes) => {
    const studentSubject = sequelize.define("Student_Subject", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return studentSubject;
};