module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("User", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        Email: {
            type: DataTypes.STRING(25),
            allowNull: false,
            unique: true
        },
        Password: {
            type: DataTypes.STRING(20),
            allowNull: false,
        },
        Type: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        Picture: {
            type: DataTypes.TEXT,
        },
        Guardian: {
            type: DataTypes.STRING(30),
        },
        Phone_1: {
            type: DataTypes.STRING(20),
        },
        Phone_2: {
            type: DataTypes.STRING(20),
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return User;
};