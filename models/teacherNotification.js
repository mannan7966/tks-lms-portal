module.exports = (sequelize, DataTypes) => {
    const Notification = sequelize.define("TeacherNotification", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Title: {
            type: DataTypes.STRING(512),
            allowNull: false,
        },
        Description: {
            type: DataTypes.STRING(512),
            allowNull: false,
        },
        SubjectID: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        ChapterID: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        TeacherID:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        Branch: {
            type: DataTypes.INTEGER(11),
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Notification;
};