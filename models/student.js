module.exports = (sequelize, DataTypes) => {
    const Student = sequelize.define("Student", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Roll_No: {
            type: DataTypes.STRING(14),
            allowNull: false,
            unique: true
        },
        Name: {
            type: DataTypes.STRING(30),
            allowNull: false,
        },
        Address: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        Class: {
            type: DataTypes.STRING(15),
            allowNull: false,
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Student;
};