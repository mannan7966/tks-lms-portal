module.exports = (sequelize, DataTypes) => {
    const Teacher = sequelize.define("Teacher", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Teacher_id: {
            type: DataTypes.STRING(14),
            allowNull: false,
            unique: true
        },
        Name: {
            type: DataTypes.STRING(30),
            allowNull: false,
        },
        Address: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Teacher;
};