module.exports = (sequelize, DataTypes) => {
    const Chapter = sequelize.define("Chapter", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Chapter_No: DataTypes.INTEGER(11),
        Name: DataTypes.STRING(512),
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Chapter;
};