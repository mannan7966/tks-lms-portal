module.exports = (sequelize, DataTypes) => {
    const Question = sequelize.define("Question", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Statement: {
            type: DataTypes.STRING(512),
            allowNull: false
        },
        A: {
            type: DataTypes.STRING(20),
            allowNull: true
        },
        B: {
            type: DataTypes.STRING(20),
            allowNull: true
        },
        C: {
            type: DataTypes.STRING(20),
            allowNull: true
        },
        D: {
            type: DataTypes.STRING(20),
            allowNull: true
        },
        Correct: {
            type: DataTypes.STRING(20),
            allowNull: true
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Question;
};