module.exports = (sequelize, DataTypes) => {
    const Subject = sequelize.define("Subject", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Name: {
            type: DataTypes.STRING(30),
            allowNull: false,
        },
        Class: {
            type: DataTypes.STRING(15),
            allowNull: false,
        },
        Marks: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        PDF: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Subject;
};