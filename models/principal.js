module.exports = (sequelize, DataTypes) => {
    const Principal = sequelize.define("Principal", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Principal_id: {
            type: DataTypes.STRING(14),
            allowNull: false,
            unique: true
        },
        Name: {
            type: DataTypes.STRING(30),
            allowNull: false,
        },
        Address: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Principal;
};