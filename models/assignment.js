module.exports = (sequelize, DataTypes) => {
    const Assignment = sequelize.define("Assignment", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Due_Date: DataTypes.STRING(512),
        Marks: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        Branch: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Assignment;
};