'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

//Associations
db.User.hasOne(db.Student, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Student.belongsTo(db.User);
////////////////////////////////////////

db.User.hasOne(db.Principal, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Principal.belongsTo(db.User);

////////////////////////////////////////

db.Branch.hasOne(db.Principal, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Principal.belongsTo(db.Branch);

////////////////////////////////////////

db.Branch.hasMany(db.Teacher, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Teacher.belongsTo(db.Branch);

////////////////////////////////////////
db.Branch.hasMany(db.Student,
  {
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  }
);
db.Student.belongsTo(db.Branch);

/////////////////////////////////////////

db.User.hasOne(db.Teacher, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Teacher.belongsTo(db.User);

/////////////////////////////////////////

db.Student.hasMany(db.Progress_Report);
db.Progress_Report.belongsTo(db.Student, {
  foreignKey: {
    allowNull: false
  }
});

db.Subject.hasMany(db.Progress_Report);
db.Progress_Report.belongsTo(db.Subject, {
  foreignKey: {
    allowNull: false
  }
});

////////////////
db.Student.hasMany(db.Student_Subject);
db.Student_Subject.belongsTo(db.Student, {
  foreignKey: {
    allowNull: false
  }
});

db.Subject.hasMany(db.Student_Subject);
db.Student_Subject.belongsTo(db.Subject, {
  foreignKey: {
    allowNull: false
  }
});

////////////////////////////////////////

db.Subject.hasMany(db.Chapter, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Chapter.belongsTo(db.Subject, {
  foreignKey: {
    allowNull: false
  },
});
////////////////////////////////////////
db.Chapter.hasMany(db.Video, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Video.belongsTo(db.Chapter, {
  foreignKey: {
    allowNull: false
  }
});

db.Subject.hasMany(db.Video, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Video.belongsTo(db.Subject, {
  foreignKey: {
    allowNull: false
  }
});


////////////////////////////////////////
db.Chapter.hasOne(db.PDF, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.PDF.belongsTo(db.Chapter, {
  foreignKey: {
    allowNull: false
  }
});
////////////////////////////////////////
db.Chapter.hasMany(db.Assignment, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Assignment.belongsTo(db.Chapter, {
  foreignKey: {
    allowNull: false
  }
});


db.Subject.hasMany(db.Assignment, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Assignment.belongsTo(db.Subject, {
  foreignKey: {
    allowNull: false
  }
});
////////////////////////////////////////
db.Chapter.hasMany(db.Quiz, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Quiz.belongsTo(db.Chapter, {
  foreignKey: {
    allowNull: false
  }
});


db.Subject.hasMany(db.Quiz, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Quiz.belongsTo(db.Subject, {
  foreignKey: {
    allowNull: false
  }
});
////////////////////////////////////////

db.Assignment.hasMany(db.Question, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Question.belongsTo(db.Assignment, {
  foreignKey: {
    allowNull: true
  }
});


db.Quiz.hasMany(db.Question, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Question.belongsTo(db.Quiz, {
  foreignKey: {
    allowNull: true
  }
});

////////////////////////////////////////




db.Quiz.hasMany(db.Marks, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Marks.belongsTo(db.Quiz, {
  foreignKey: {
    allowNull: false
  }
});

///////////////////////////////////////////

db.Student.hasMany(db.Marks, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Marks.belongsTo(db.Student, {
  foreignKey: {
    allowNull: false
  }
});

///////////////////////////////////////////
db.Student.hasMany(db.Marks, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Marks.belongsTo(db.Student, {
  foreignKey: {
    allowNull: false
  }
});

////////////////////////////////////////////
db.Student.hasMany(db.AssignmentDocument, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.AssignmentDocument.belongsTo(db.Student, {
  foreignKey: {
    allowNull: false
  }
});
///////////////////////////////////////////
db.Assignment.hasMany(db.AssignmentDocument, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.AssignmentDocument.belongsTo(db.Assignment, {
  foreignKey: {
    allowNull: false
  }
});
////////////////////////////////////////////
///////////////////////////////////////////
db.Question.hasMany(db.AssignmentDocument, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.AssignmentDocument.belongsTo(db.Question, {
  foreignKey: {
    allowNull: false
  }
});
////////////////////////////////////////////
db.Teacher.hasMany(db.Quiz, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Quiz.belongsTo(db.Teacher, {
  foreignKey: {
    allowNull: false
  }
});

db.Teacher.hasMany(db.Assignment, {
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
});
db.Assignment.belongsTo(db.Teacher, {
  foreignKey: {
    allowNull: false
  }
});

module.exports = db;
