module.exports = (sequelize, DataTypes) => {
    const teacherVideo = sequelize.define("Teacher_Video", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Title: DataTypes.STRING(512),
        Link: DataTypes.STRING(512),
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return teacherVideo;
};