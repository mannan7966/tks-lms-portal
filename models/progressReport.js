module.exports = (sequelize, DataTypes) => {
    const ProgressReport = sequelize.define("Progress_Report", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Marks: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        Total_Marks: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        Term:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return ProgressReport;
};