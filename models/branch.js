module.exports = (sequelize, DataTypes) => {
    const Branch = sequelize.define("Branch", {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        Code: {
            type: DataTypes.STRING(12),
            allowNull: false,
        },
        Name: {
            type: DataTypes.STRING(30),
            allowNull: false,
        },
        Address: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        Status: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    });
    return Branch;
};