require("dotenv").config();
const express = require("express");
let app = express();
const db = require("./models");
const morgan = require("morgan");
const helmet = require("helmet");
const multer = require('multer');
const cors = require("cors");



require("./auth/passport");

const PORT = process.env.PORT || 9000;


const api = require("./routes")


app.use(function (req, res, next) {
    //Enabling CORS
    res.header("Access-Control-Allow-Origin", "*");
    next();
});

app.use('/public', express.static('public'));
app.use(express.json({ limit: '500mb' }));
app.use(express.urlencoded({ limit: '500mb', extended: true }));
app.use(morgan("dev"));
app.use(helmet());

app.use(cors());



const notificationRoutes = require("./routes/notificationRoutes");
const userRoutes = require("./routes/userRoutes");
const login = require("./routes/login");
const student = require("./routes/student");
const progressReport = require("./routes/progressReport");
const subject = require("./routes/subject");
const studentSubject = require("./routes/studentSubject");
const chapter = require("./routes/chapter");
const video = require("./routes/video");
const pdf = require("./routes/pdf");
const assignment = require("./routes/assignment");
const quiz = require("./routes/quiz");
const question = require("./routes/question");
const teacher = require("./routes/teacher");
const teacherVideo = require("./routes/teacherVideo");
const marks = require("./routes/marks");
const principal = require("./routes/principal");
const branch = require("./routes/branch");
const document = require("./routes/document");
const assignmentDocument = require("./routes/assignmentDocuments");
const TeacherNotification = require("./routes/teacherNotification");



app.use("/api/notificationRoutes", notificationRoutes);
app.use("/api/userRoutes", userRoutes);
app.use("/api/login", login);
app.use("/api/student", student);
app.use("/api/progressReport", progressReport);
app.use("/api/subject", subject);
app.use("/api/studentSubject", studentSubject);
app.use("/api/chapter", chapter);
app.use("/api/video", video);
app.use("/api/pdf", pdf);
app.use("/api/assignment", assignment);
app.use("/api/quiz", quiz);
app.use("/api/question", question);
app.use("/api/teacher", teacher);
app.use("/api/teacherVideo", teacherVideo);
app.use("/api/marks", marks);
app.use("/api/principal", principal);
app.use("/api/branch", branch);
app.use("/api/document", document);
app.use("/api/assignmentDocument", assignmentDocument);
app.use("/api/teacherNotification", TeacherNotification);



db.sequelize.sync().then(() => {
    app.listen(PORT, () => {
        console.log("Listening on:http://localhost:" + PORT);
    });
});

